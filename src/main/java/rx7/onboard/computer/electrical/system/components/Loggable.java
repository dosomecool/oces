package rx7.onboard.computer.electrical.system.components;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import rx7.onboard.computer.electrical.system.constants.Constants.LogKey;

public interface Loggable {
		
	public String getName();
	public String getState();
	public double getValue();
	
	public default Map<String, String> log() {
		Map<String,String> log = new HashMap<String, String>();
		log.put(LogKey.DATE.toString(), new Date().toString());
		log.put(LogKey.NAME.toString(), this.getName());
		log.put(LogKey.STATE.toString(), this.getState());
		log.put(LogKey.VALUE.toString(), String.valueOf(this.getValue()));
		return log;
	}
}
