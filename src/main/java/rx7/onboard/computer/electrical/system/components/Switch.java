package rx7.onboard.computer.electrical.system.components;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.InputEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class Switch extends Component implements Functionable {

	private Indicator indicator;
	private Timeline timeline; 

	private boolean isOn = false;
	private boolean blink = false;

	public Switch(Indicator indicator, String name) {
		super(name);
		this.indicator = indicator;
	}

	private void blinkAnimation() {
		timeline = new Timeline(new KeyFrame(Duration.seconds(0.85), evt -> indicator.lightOn()),
				new KeyFrame(Duration.seconds(0.7), evt -> indicator.lightOff()));
		timeline.setCycleCount(50);
		timeline.play();
	}

	private void turnOnAnimation() {
		indicator.lightOn();
	}

	private void turnOffAnimation() {
		isOn = false;
		indicator.lightOff();

		if(timeline != null) {
			timeline.stop();
		}
	}

	@Override
	public void turnOn() {
		turnOnAnimation();
	}

	@Override
	public void turnOff() {
		turnOffAnimation();
	}

	@Override
	public void ignitionOn() {
		((Node) indicator.getLight()).addEventHandler(MouseEvent.MOUSE_PRESSED, onHandler);
	}

	@Override
	public void ignitionOff() {
		((Node) indicator.getLight()).removeEventHandler(MouseEvent.MOUSE_PRESSED, onHandler);
	}
	
	@Override
	public void onError() {
		System.out.println("Error");
	}

	EventHandler<InputEvent> onHandler = new EventHandler<InputEvent>() {

		@Override
		public void handle(InputEvent event) {
			if (((MouseEvent) event).getClickCount() == 1 && isOn == false ) {
				indicator.lightOn();
				isOn = true;

				Platform.runLater(new Runnable() {

					@Override
					public void run() {

						if(blink) {
							blinkAnimation();
						} else {
							turnOnAnimation();
						}
					}
				});

			} else if (((MouseEvent) event).getClickCount() == 1 && isOn == true) { 
				turnOffAnimation();
			}
			event.consume();
		}};
		
		public Switch blinker() {
			this.blink = true;
			return this;
		}
}
