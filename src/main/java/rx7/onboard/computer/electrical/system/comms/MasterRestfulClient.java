package rx7.onboard.computer.electrical.system.comms;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import rx7.onboard.computer.electrical.system.domain.Message;

// https://grokonez.com/java-integration/use-spring-resttemplate-client-consuming-restful-webservice
// https://howtodoinjava.com/spring-restful/spring-restful-client-resttemplate-example/

// rpi0
// 192.168.0.100

// rpi1
// 192.168.0.101


public class MasterRestfulClient {

	RestTemplate restTemplate;

	public MasterRestfulClient() {
		restTemplate = new RestTemplate();
	}

	public void getMessage() {

		System.out.println("Begin /GET request!");

		String getUrl = "http://localhost:8080/get?id=1&name='Mary'&age=20";

		ResponseEntity<Message> getResponse = restTemplate.getForEntity(getUrl, Message.class);


		if(getResponse.getBody() != null){
			System.out.println("Response for Get Request: " + getResponse.getBody().toString());    
		}else{
			System.out.println("Response for Get Request: NULL");
		}
	}

	public static void postMessage() {
		
		final String uri = "http://localhost:8080/springrestexample/employees";

		Message message = new Message("Hello"); 

		RestTemplate restTemplate = new RestTemplate();
		Message result = restTemplate.postForObject( uri, message, Message.class);

		System.out.println(result);
	}
	
	// TODO: create a switch for RPI1 and RPI2
}
