package rx7.onboard.computer.electrical.system.domain;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Message {

	private String message;
	
	public Message() {		
	}

	public Message(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Message: ").append(message);
		
		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
}
