package rx7.onboard.computer.electrical.system.mainscreen;

import rx7.onboard.computer.electrical.system.domain.Message;

public interface Controller {

	void update(Message message);
}
