package rx7.onboard.computer.electrical.system.mainscreen;

import static rx7.onboard.computer.electrical.system.constants.Constants.Colors.BLUE;
import static rx7.onboard.computer.electrical.system.constants.Constants.Colors.GRAY;
import static rx7.onboard.computer.electrical.system.constants.Constants.Colors.GREEN;
import static rx7.onboard.computer.electrical.system.constants.Constants.Colors.YELLOW;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import de.jensd.fx.glyphs.weathericons.WeatherIconView;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import rx7.onboard.computer.electrical.system.comms.MasterRestfulServer;
import rx7.onboard.computer.electrical.system.components.Indicator;
import rx7.onboard.computer.electrical.system.components.Switch;
import rx7.onboard.computer.electrical.system.domain.Message;
import rx7.onboard.computer.electrical.system.lockscreen.LockScreen;
import rx7.onboard.computer.electrical.system.main.Configuration;
import rx7.onboard.computer.electrical.system.service.MessageProcessor;

// Spring Boot with JavaFX
// Source: https://wimdeblauwe.wordpress.com/2017/09/18/using-spring-boot-with-javafx/

// Touch events in JavaFX
// Source: https://docs.oracle.com/javase/8/javafx/events-tutorial/gestures.htm#JFXED166

// Build with Mqaven
// Source: https://openjfx.io/openjfx-docs/#maven

// Liberica JDK 11
// https://www.bell-sw.com/java.html

// e(fx) plugin for Java 11
// https://download.eclipse.org/efxclipse/updates-released/3.4.1/site/

public class MasterScreen extends AnchorPane implements Controller {

	@FXML
	private AnchorPane root;

	@FXML
	private StackPane stackPane;

	@FXML
	private AnchorPane lockAnchor;

	@FXML
	private AnchorPane mainAnchor;

	@FXML
	private FontAwesomeIconView leftSignal;

	@FXML
	private FontAwesomeIconView rightSignal;
	
	@FXML
	private WeatherIconView lowBeam;

	@FXML
	private WeatherIconView highBeam;

	@FXML
	private AnchorPane topAnchor;

	@FXML
	private HBox topHBox;

	@FXML
	private FontAwesomeIconView burger;
	
	// Lock screen
	private LockScreen lockScreen = new LockScreen(this);
	
	// Switches and indicators 
	private Switch leftTurnSignal;  
	private Switch rightTurnSignal;
	
	private Indicator leftTurnIndicator;
	private Indicator rightTurnIndicator;
	
	private Switch lowBeamLights; 
	private Switch highBeamLights;
	
	private Indicator lowBeamIndicator;
	private Indicator highBeamIndicator;
	
	private TranslateTransition translateTopAnchor;
	private int direction = 1;
	private final static double OFFSET = 75;
	
	private Configuration configuration;
	
	final BlockingQueue<Message> messageQueue = new LinkedBlockingQueue<Message>();
	
	private MasterRestfulServer masterRestfulServer = new MasterRestfulServer();
	
	private MessageProcessor messageProcessor = new MessageProcessor(); 

	public MasterScreen() {
		
		configuration = Configuration.getInstance();
		
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("mainscreen.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(masterRestfulServer != null) {
			masterRestfulServer.setQueue(messageQueue);
			System.out.print("m server is not null");
		} else {
			System.out.print("m server is null");
		}
				
		messageProcessor.setController(this);
		messageProcessor.setQueue(messageQueue);
		
	}

	public void pushMenu() {
		direction = direction * -1; // first time: 1 * -1 = -1;
		translateTopAnchor.setFromY(0);
		translateTopAnchor.setToY(-OFFSET);
		translateTopAnchor.play();		
	}

	public void pullMenu() {
		direction = direction * -1;
		translateTopAnchor.setFromY(-OFFSET);
		translateTopAnchor.setToY(0);
		translateTopAnchor.play();
	}

	@SuppressWarnings("static-access")
	@FXML
	public void initialize() {
		
		leftTurnIndicator = new Indicator(leftSignal)
				.setOnColor(GREEN.getColor())
				.setOffColor(GRAY.getColor());
		
		leftTurnSignal = new Switch(leftTurnIndicator, "Left Turn Signal")
				.blinker();
		
		rightTurnIndicator = new Indicator(rightSignal)
				.setOnColor(GREEN.getColor())
				.setOffColor(GRAY.getColor());
		
		rightTurnSignal = new Switch(rightTurnIndicator, "Right Turn Signal")
				.blinker();
		
		leftTurnSignal.ignitionOn();
		rightTurnSignal.ignitionOn();
		
		lowBeamIndicator = new Indicator(lowBeam)
				.setOnColor(YELLOW.getColor())
				.setOffColor(GRAY.getColor());
		
		lowBeamLights = new Switch(lowBeamIndicator, "Right Turn Signal");
		
		highBeamIndicator = new Indicator(highBeam)
				.setOnColor(BLUE.getColor())
				.setOffColor(GRAY.getColor());
		
		highBeamLights = new Switch(highBeamIndicator, "Right Turn Signal");
		
		lowBeamLights.ignitionOn();
		highBeamLights.ignitionOn();
		
		// Start in lock screen
		lockAnchor.getChildren().add(lockScreen);
		lockAnchor.setLeftAnchor(lockScreen, 0.0);  
		lockAnchor.setRightAnchor(lockScreen, 0.0);  
		lockAnchor.setTopAnchor(lockScreen, 0.0);  
		lockAnchor.setBottomAnchor(lockScreen, 0.0); 
		toFront(lockAnchor);

		// Push up top menu
		translateTopAnchor = new TranslateTransition(Duration.millis(500), topAnchor);
		pushMenu();

		topAnchor.addEventHandler(MouseEvent.MOUSE_PRESSED, (e)-> {

			if (e.getClickCount() == 1 && direction == 1 ){
				pushMenu();
			} else if (e.getClickCount() == 1 && direction == -1) { 
				pullMenu();
			}
		});

		// TODO: Implement this when we get the screen
		//		burger.setOnTouchReleased(new EventHandler<TouchEvent>() {
		//			@Override public void handle(TouchEvent event) {
		//
		//				System.out.println("Opening menu ...");
		//				event.consume();
		//			}
		//		});
	}

	private void toFront(Node node) {
		ObservableList<Node> childs = this.stackPane.getChildren();

		for(Node n : childs) {
			if(n.equals(node)) {

				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						node.toFront();
					}
				});
			}
		}
	}

	public void unlock() {
		toFront(mainAnchor);
		toFront(topAnchor);
	}

	public void update(Message message) {
		
		// this should not be the raw message 
		// this should be the processed message
		
	}
}
