package rx7.onboard.computer.electrical.system.modules;

import java.util.List;

import rx7.onboard.computer.electrical.system.components.Component;

public abstract class Module {

	private List<Component> components; 
	
	public abstract void on();
	public abstract void off();

	public List<Component> getComponents() {
		return components;
	}

	public void setComponents(List<Component> components) {
		this.components = components;
	}
}
