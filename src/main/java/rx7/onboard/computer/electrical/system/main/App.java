package rx7.onboard.computer.electrical.system.main;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.sun.javafx.application.LauncherImpl;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import rx7.onboard.computer.electrical.system.mainscreen.MasterScreen;
import rx7.onboard.computer.electrical.system.mainscreen.PheripheralScreen;

// https://stackoverflow.com/questions/36961054/can-javafx-be-used-on-raspberry-pi

// close javafx program
// https://stackoverflow.com/questions/24538236/why-does-my-javafx-application-not-have-a-frame-when-run-on-my-raspberrypi

// with frame
// https://raspberrypi.stackexchange.com/questions/75451/javafx-on-raspberry-pi-3-fullscreen-black-bars

@SpringBootApplication
@ComponentScan(basePackages="rx7.onboard.computer.electrical.system")
public class App extends Application {

	private ConfigurableApplicationContext context;
	private Parent rootNode;

	private Configuration configuration = Configuration.getInstance(); 
	private String hostname = "Unknown";
	
	@Override
	public void init() throws Exception {
		SpringApplicationBuilder builder = new SpringApplicationBuilder(App.class);
		context = builder.run(getParameters().getRaw().toArray(new String[0]));
		
		try
		{
		    InetAddress address;
		    address = InetAddress.getLocalHost();
		    hostname = address.getHostName();
		}
		catch (UnknownHostException ex)
		{
		    System.out.println("Hostname can not be resolved");
		}
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
//		Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
//		double width = visualBounds.getWidth();
//		double height = visualBounds.getHeight();

//		primaryStage.setScene(new Scene(rootNode, width, height));
//		primaryStage.centerOnScreen();
//		primaryStage.show();
		
//		String role = "Unknown";
//		
//		if((role = configuration.getConfig(hostname, "role")).equals("master")) {
//			final MasterScreen mainScreen = new MasterScreen();
//			System.out.println("Hostname: " + hostname + " role: " + role);
//		} else if(role.equals("pheripheral")){
//			final PheripheralScreen mainScreen = new PheripheralScreen();
//			System.out.println("Hostname: " + hostname + " role: " + role);
//		} else {
//			System.out.print("Something is wrong with hostname");
//			Platform.exit();
//		}
		
//		final PheripheralScreen mainScreen = new PheripheralScreen();
		
		final MasterScreen mainScreen = new MasterScreen();
        final Scene scene = new Scene(mainScreen);
        
        primaryStage.setTitle("OCES");
        primaryStage.setScene(scene);
        primaryStage.centerOnScreen();
        primaryStage.setResizable(true);
        primaryStage.show();
        stop();
	}
	
	public static void main(String[] args) {
		LauncherImpl.launchApplication(App.class, args);
	}

	@Override
	public void stop() throws Exception {
		System.out.print("stopped");
		context.close();
	}
}

