package rx7.onboard.computer.electrical.system.modules;

public class TurnOffModule implements Request {

	private Module module; 

	public TurnOffModule(Module module) {
		this.module = module;
	}

	public void execute() {
		module.off();
	}
}
