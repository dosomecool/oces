package rx7.onboard.computer.electrical.system.constants;

import javafx.scene.paint.Color;

public class Constants {

	public static final String CONFIG_FILE = "\\configuration.ini"; // System.getProperties().get("user.dir") 

	public static enum Turn {
		
		LEFT("Left"), RIGHT("Right");
		
		private String turn;
		
		Turn(String turn) {
			this.turn = turn;
		}
		
	    public String request() {
	        return turn;
	    }
	}
	
	public static enum State {
		
		INITIALIZED("Initialized"), ON("On"), OFF("Off"), ERROR("Unknown");
		
		private String state;
		
		State(String state) {
			this.state = state;
		}
		
	    public String getState() {
	        return state;
	    }
	}
	
	public static enum LogKey {
		
		DATE("Date"), NAME("Name"), STATE("State"), VALUE("Value");
		
		private String logKey;
		
		LogKey(String logKey) {
			this.logKey = logKey;
		}
		
	    public String getKey() {
	        return logKey;
	    }
	}
	
	public static enum Colors {
		
		GREEN(Color.rgb(0,255,0)),
		YELLOW(Color.rgb(245,241,6)),
		BLUE(Color.rgb(0,51,255)),
		GRAY(Color.rgb(105,105,105));
			
		private Color color;
		
		private Colors(Color color) {
			this.color = color;
		}

		public Color getColor() {
			return color;
		}
	}
}
