package rx7.onboard.computer.electrical.system.mainscreen;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.springframework.beans.factory.annotation.Autowired;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import rx7.onboard.computer.electrical.system.comms.MasterRestfulServer;
import rx7.onboard.computer.electrical.system.comms.PeripheralRestfulServer;
import rx7.onboard.computer.electrical.system.domain.Message;
import rx7.onboard.computer.electrical.system.main.Configuration;
import rx7.onboard.computer.electrical.system.service.MessageProcessor;

public class PheripheralScreen extends AnchorPane implements Controller {

	private Configuration configuration;
	
    @FXML
    private AnchorPane root;

    @FXML
    private StackPane stackPane;

    @FXML
    private AnchorPane lockAnchor;

    @FXML
    private AnchorPane mainAnchor;

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private TextArea textArea;

    @FXML
    private AnchorPane topAnchor;

    @FXML
    private HBox topHBox;
    
    private HBox logBox;
    
    private List<String> logList = new ArrayList<String>(); 
    
	final BlockingQueue<Message> messageQueue = new LinkedBlockingQueue<Message>();
	
	@Autowired
	private PeripheralRestfulServer peripheralRestfulServer;
	
	private MessageProcessor messageProcessor = new MessageProcessor();;

	public PheripheralScreen() {

		configuration = Configuration.getInstance();
		
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("pheripheralscreen.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(peripheralRestfulServer != null) {
			peripheralRestfulServer.setQueue(messageQueue);
		} else {
			System.out.print("p-server is null");
		}
		
		messageProcessor.setController(this);
		messageProcessor.setQueue(messageQueue);
	}
	
	@FXML
	public void initialize() {
		textArea.setStyle("-fx-control-inner-background:#000000; -fx-font-family: Consolas; -fx-highlight-fill: #00ff00; -fx-highlight-text-fill: #000000; -fx-text-fill: #00ff00; ");
	}

	@Override
	public void update(Message message) {
		textArea.appendText("\n");
		textArea.appendText(message.getMessage());
	}
}
