package rx7.onboard.computer.electrical.system.comms;

import java.util.concurrent.BlockingQueue;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import rx7.onboard.computer.electrical.system.domain.Message;

@CrossOrigin(origins = { "http://localhost:3000", 
		"http://192.168.1.15:3000", 
		"http://127.0.0.1:3000", 
		"http://drifftspace.com.s3-website.us-east-2.amazonaws.com", 
"http://drifftspace.com.s3-website.us-east-2.amazonaws.com:80" }, maxAge = 3000)
@RestController
public class MasterRestfulServer {
	
	private BlockingQueue<Message> messageQueue;

	@RequestMapping(value = "/master-inbox", produces = MediaType.APPLICATION_JSON_VALUE,  method = RequestMethod.GET)
	public String getAllEmployeesJSON() {
	    return "jsonTemplate";
	}

	@RequestMapping(value = "/master-outbox", method = RequestMethod.POST)
	public ResponseEntity<String> createMessage(@RequestBody Message message) {
	    System.out.println(message);
	    submitMessage(message);
	    return new ResponseEntity<String>(HttpStatus.CREATED);
	}
	
	public void submitMessage(Message message) { 
		try {
			messageQueue.put(message);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void setQueue(BlockingQueue<Message> queue) {
		this.messageQueue = queue; 
	}
}