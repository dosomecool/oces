package rx7.onboard.computer.electrical.system.modules;

public class TestMain {

	// implementation
	
    Module module = new Module() {
    	
    	@Override
    	public void on() {
    		// logic
    	}
    	
    	@Override
    	public void off() {
    		// logic
    	}
    	
    };

    TurnOnModule turnOnModule = new TurnOnModule(module);
    TurnOffModule turnOffModule = new TurnOffModule(module);


    
    public void init() {
        
    	ModuleController broker = new ModuleController();
        broker.receive(turnOnModule);
        broker.receive(turnOffModule);

        broker.commit();
    }
	
}
