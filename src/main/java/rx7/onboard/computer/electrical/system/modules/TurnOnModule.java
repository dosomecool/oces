package rx7.onboard.computer.electrical.system.modules;

public class TurnOnModule implements Request {

	private Module module; 

	public TurnOnModule(Module module) {
		this.module = module;
	}

	public void execute() {
		module.on();
	}
}
