package rx7.onboard.computer.electrical.system.modules;

public interface Request {
	
	public void execute();
}
