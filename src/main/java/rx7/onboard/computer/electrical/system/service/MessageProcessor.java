package rx7.onboard.computer.electrical.system.service;

import java.util.concurrent.BlockingQueue;

import javafx.application.Platform;
import rx7.onboard.computer.electrical.system.domain.Message;
import rx7.onboard.computer.electrical.system.mainscreen.Controller;

public class MessageProcessor implements Runnable {

	private BlockingQueue<Message> messageQueue;
	private Message message;
	
	private Controller controller; 
	
	public MessageProcessor() {
	}
	
	@Override
	public void run() {
		
		try{
			while(!(message = messageQueue.take()).getMessage().equals("exit")) {
								
				Platform.runLater(new Runnable() {

					@Override
					public void run() {
					
						// process the message 
						// update screen here
						// controller.update(); 
					}
				});
			}

		} catch(InterruptedException e) {
			e.printStackTrace(); 
		} 
	}
	
	public void setQueue(BlockingQueue<Message> queue) {
		this.messageQueue = queue;
	}
	
	public void setController(Controller controller) {
		this.controller = controller;
	}
}
