package rx7.onboard.computer.electrical.system.components;

public interface Functionable {
	
	public void turnOn();
	public void turnOff();
	public void ignitionOn();
	public void ignitionOff();
	
	public void onError();
}
