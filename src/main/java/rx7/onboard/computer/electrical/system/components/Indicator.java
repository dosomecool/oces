package rx7.onboard.computer.electrical.system.components;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import de.jensd.fx.glyphs.weathericons.WeatherIconView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import static rx7.onboard.computer.electrical.system.constants.Constants.Colors.*;

public class Indicator {

	private FontAwesomeIconView typeOne = null;
	private WeatherIconView typeTwo = null;
	
	private Color onColor = GREEN.getColor();
	private Color offColor = GRAY.getColor();
	
	public Indicator(FontAwesomeIconView type) {
		this.typeOne = type;
	}
	
	public Indicator(WeatherIconView type) {
		this.typeTwo = type;
	}
	
	public Indicator setOnColor(Color color) {
		this.onColor = color;
		return this;
	}
	
	public Indicator setOffColor(Color color) {
		this.offColor = color;
		return this;
	}

	public void lightOn() {
		((Shape) getLight()).setFill(onColor);
	}
	
	public void lightOff() {
		((Shape) getLight()).setFill(offColor);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getLight() {
		if(typeOne != null) {
			return (T) typeOne;
		}
		
		if(typeTwo != null) {
			return (T) typeTwo;
		}
		return null;
	}
}
