package rx7.onboard.computer.electrical.system.main;

//import org.apache.log4j.Logger;
import org.ini4j.InvalidFileFormatException;
import org.ini4j.Wini;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Configuration {
	
	// SOURCE: http://www.javenue.info/post/40

//    private static Logger LOGGER = Logger.getLogger(Configuration.class);
    		
    private static class HolderLazySingleton {
        private static Configuration instance = new Configuration();
    }

    private Wini ini = null;
    
    // DEFAULT LOCATION OF "CONFIGURATION.INI"
    // TODO: this can be moved to a constants file
//    private final static String DEFAULT_CONFIGURATION_FILE = System.getProperties().get("user.dir") + "\\Configuration.ini";
    
//    private final static String DEFAULT_CONFIGURATION_FILE = Thread.currentThread().getContextClassLoader().getResource("CONFIGURATION").getFile();
    private final static String DEFAULT_CONFIGURATION_FILE = System.getProperties().get("user.dir") +  "\\configuration.ini";
    private final static String DEFAULT_WORKING_DIRECTORY = System.getProperties().get("user.dir") + "\\_temp\\";
    
    private Configuration() { 
    	
    	System.out.println("\n test:" + DEFAULT_CONFIGURATION_FILE + "\n");

//    	File configurationFile = new File(DEFAULT_CONFIGURATION_FILE);
//
//    	//Create the file
//    	try {
//    		if (configurationFile.createNewFile()){
//
//    			BufferedWriter writer = Files.newBufferedWriter(Paths.get(DEFAULT_CONFIGURATION_FILE));
//
//    			writer.write("[user_preferences]");
//    			writer.newLine();
//    			writer.write("working_directory = " + DEFAULT_WORKING_DIRECTORY);
//    			writer.newLine();
//    			writer.newLine();
//
//    			writer.close();
//    			configurationFile.canExecute();
//
//    		}else{
//    			System.out.println("Configuration.ini is already there");
//    		}
//    		
    		try {
				ini = new Wini(new File(DEFAULT_CONFIGURATION_FILE));
			} catch (InvalidFileFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("ohhh yea same error");
//				e.printStackTrace();
			}
//
//    	} catch (IOException e) {
//    		e.printStackTrace();
//    	}	
    }

    public static Configuration getInstance() {
        return HolderLazySingleton.instance;
    }

    /**
     * @param section
     * @param field
     * @return
     */
    public String getConfig(String section, String field) {

        String readValue = null;

        if (ini.get(section, field) != null) {
            readValue = ini.get(section, field);
        } else {
            // TODO: What should happen
        }
        return readValue;
    }

    /**
     * @param section
     * @param field
     * @param value
     */
    public void setConfig(String section, String field, String value) {

        try {
            ini.put(section, field, value);
            ini.store();
        } catch (Exception e1) {
//            LOGGER.error(value + " could not be stored.", e1);         
        }
    }
}