package rx7.onboard.computer.electrical.system.modules;

import java.util.ArrayList;
import java.util.List;

public class ModuleController {

	private List<Request> requestList = new ArrayList<Request>(); 

	public void receive(Request request){
		requestList.add(request);		
	}

	public void commit(){

		for (Request r : requestList) {
			r.execute();
		}
		requestList.clear();
	}
}
