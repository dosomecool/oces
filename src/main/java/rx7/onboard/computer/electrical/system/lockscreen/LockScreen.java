package rx7.onboard.computer.electrical.system.lockscreen;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.util.Duration;
import rx7.onboard.computer.electrical.system.mainscreen.MasterScreen;

public class LockScreen extends GridPane {

	@FXML
	private GridPane comboGrid;

	@FXML
	private Label inputOne;

	@FXML
	private Label inputTwo;

	@FXML
	private Label inputThree;

	@FXML
	private Label inputFour;

	@FXML
	private Button buttonOne;

	@FXML
	private Button buttonTwo;

	@FXML
	private Button buttonThree;

	@FXML
	private Button buttonFour;

	@FXML
	private Button buttonFive;

	@FXML
	private Button buttonSix;

	@FXML
	private Button buttonSeven;

	@FXML
	private Button buttonEight;

	@FXML
	private Button buttonNine;

	@FXML
	private Button buttonZero;

	@FXML
	private Button buttonOk;

	@FXML
	private Button buttonClear;

	List<Button> padList = new ArrayList<Button>();
	private static final int ONE = 1;
	private static final int TWO = 2;
	private static final int THREE = 3;
	private static final int FOUR = 4;
	private static final int FIVE = 5;
	private static final int SIX = 6;
	private static final int SEVEN = 7;
	private static final int EIGHT = 8;
	private static final int NINE = 9;
	private static final int ZERO = 0;

	List<Label> comboList = new ArrayList<Label>();

	private final static int MAX_COUNT = 4;
	private int count = 0;
	private Timeline wrongPasswordTimeline; 

	private final static String RED = "#FF0000";
	private final static String WHITE = "#FFFFFF";

	// TODO: Remove this when adding configuration
	private List<Integer> password = new ArrayList<Integer>( 
			Arrays.asList(1, 2, 3, 4));
	private List<Integer> userInput = new ArrayList<Integer>();

	private MasterScreen controller = null;

	public LockScreen(MasterScreen controller) {

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("lockscreen.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}

		this.controller = controller;
	}

	@FXML
	public void initialize() {

		// Create a pad list
		padList.add(buttonOne);
		padList.add(buttonTwo);
		padList.add(buttonThree);
		padList.add(buttonFour);
		padList.add(buttonFive);
		padList.add(buttonSix);
		padList.add(buttonSeven);
		padList.add(buttonEight);
		padList.add(buttonNine);

		// Create a combo list
		comboList.add(inputOne);
		comboList.add(inputTwo);
		comboList.add(inputThree);
		comboList.add(inputFour);

		buttonOne.setOnAction(e -> {
			input(ONE);
		});

		buttonTwo.setOnAction(e -> {
			input(TWO);
		});

		buttonThree.setOnAction(e -> {
			input(THREE);
		});

		buttonFour.setOnAction(e -> {
			input(FOUR);
		});

		buttonFive.setOnAction(e -> {
			input(FIVE);
		});

		buttonSix.setOnAction(e -> {
			input(SIX);
		});

		buttonSeven.setOnAction(e -> {
			input(SEVEN);
		});

		buttonEight.setOnAction(e -> {
			input(EIGHT);
		});

		buttonNine.setOnAction(e -> {
			input(NINE);
		});

		buttonZero.setOnAction(e -> {
			input(ZERO);
		});

		buttonOk.setOnAction(e -> {
			unlock();
		});

		buttonClear.setOnAction(e -> {
			clear();
		});
	}

	private void input(Integer input) {
		count = count + 1;

		if(count <= MAX_COUNT) {
			comboList.get(count-1).setText("*");
			userInput.add(input);
		}
	}

	private void unlock() {

		if(userInput.equals(password)) {
			clear();
			controller.unlock();
		} else {
			playAnimation();
			clear();
			userInput.clear();
		}
	}

	private void clear() {
		count = 0;
		
		for(Label l : comboList) {
			l.setText("");
		}
	}

	private void setComboListFill(String color) {

		for(Label l : comboList) {
			l.setStyle("-fx-background-color: " + color +";-fx-border-color: #000000");
		}
	}

	private void playAnimation() {

		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				wrongPasswordTimeline = new Timeline(
						new KeyFrame(Duration.seconds(0.1), evt -> setComboListFill(RED)),
						new KeyFrame(Duration.seconds(0.3), evt -> setComboListFill(WHITE)));
				wrongPasswordTimeline.setCycleCount(2);
				wrongPasswordTimeline.play();
			}
		});
	}
}

