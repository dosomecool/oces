package rx7.onboard.computer.electrical.system.components;

public abstract class Component implements Loggable {

	private String name;
	private String state = null;
	private Double value = null;

	public Component(String name) {
		this.name = name;
	}
	
	public Component(String name, String state) {
		this.name = name;
		this.state = state;
	}
	
	public Component(String name, String state, Double value) {
		this.name = name;
		this.state = state;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public abstract void turnOn();
	public abstract void turnOff();
	public abstract void ignitionOn();
	public abstract void ignitionOff();
	public abstract void onError();
}
