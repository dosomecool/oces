package rx7.onboard.computer.electrical.system.comms;

import java.util.concurrent.BlockingQueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import rx7.onboard.computer.electrical.system.domain.Message;

@CrossOrigin(origins = { "http://192.168.0.100:8080"}, maxAge = 3000)
@RestController
public class PeripheralRestfulServer {
	
	private BlockingQueue<Message> messageQueue;

	@RequestMapping(value = "/pheripheral-inbox", produces = MediaType.APPLICATION_JSON_VALUE,  method = RequestMethod.GET)
	public String getLogs() {
	    return "all logs from pheripheral server";
	}

	@RequestMapping(value = "/pheripheral-outbox", method = RequestMethod.POST)
	public ResponseEntity<String> createMessage(@RequestBody Message message)
	{
	    System.out.println(message);
	    submitMessage(message);
	    return new ResponseEntity<String>(HttpStatus.CREATED);
	}
		
	public void submitMessage(Message message) { 
		try {
			messageQueue.put(message);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void setQueue(BlockingQueue<Message> queue) {
		this.messageQueue = queue; 
	}
}
